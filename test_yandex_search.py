from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
import allure


@allure.title('First test')
#@allure.severity(Severity.BLOCKER)
def test_yandex_search():
    driver = WebDriver(executable_path='C://selenium//chromedriver.exe')
    with allure.step('Открываем страницу поиска'):
        driver.get('https://ya.ru')

    with allure.step('Ищем market'):
        search_input = driver.find_element_by_xpath('//input[@id="text"]')
        search_button = driver.find_element_by_xpath('//div[@class="search2__button"]//button[@type="submit"]')
        search_input.send_keys('market.yandex.ru')
        search_button.click()

    def check_results_count(driver):
        inner_search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')
        return len(inner_search_results) >= 10

    with allure.step('Ожидаем количество результатов больше 10'):
        WebDriverWait(driver, 5, 0.5).until(check_results_count, 'Количество результатов поиска меньше 10')

    with allure.step('Переходим по ссылке первого результата'):
        search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')
        link = search_results[0].find_element_by_xpath('.//h2/a')
        link.click()

    driver.switch_to.window(driver.window_handles[1])
    with allure.step('Check title correct'):
        assert driver.title == 'Яндекс.Маркет — выбор и покупка товаров из проверенных интернет-магазинов'


##        def check_result_count(driver):
##  return  len(inner_search_results) == 10
##  WebDriverWait(driver, 5, 0.5).until(check_result_count)
##     search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')

 ##   print(None)
