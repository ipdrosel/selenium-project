from selenium.webdriver.chrome.webdriver import WebDriver


class TestXUnitClass:
    driver: WebDriver

    def setup_method(self, method):
        self.driver = WebDriver(executable_path='C://selenium//chromedriver.exe')
        self.driver.implicitly_wait(3)

    def teardown_method(self, method):
        self.driver.close()

    def test_xunit_class(self):
        self.driver.get('http://google.com')
        import time
        time.sleep(3)
        assert True

    def test_xunit_class_2(self):
        self.driver.get('http://ya.ru')
        import time
        time.sleep(3)
        assert True
