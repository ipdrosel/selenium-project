from selenium.webdriver.chrome.webdriver import WebDriver

driver: WebDriver = None

def setup_module():
    print('setup module started')
    global driver
    driver = WebDriver(executable_path='C://selenium//chromedriver.exe')
    driver.implicitly_wait(3)

def teardown_module():
    print('teardown module started')
    global driver
    driver.close()


def test_module_setup_driver():
    print('invocation module started')
    driver.get('http://google.com')
    import time
    time.sleep(3)
    assert True